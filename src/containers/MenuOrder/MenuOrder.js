import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchMenu} from "../../repository/actions/menuActions";
import MenuItem from "../../components/MenuItem/MenuItem";
import Spinner from "../../components/UI/Spinner/Spinner";
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import {addDish, removeDish, setPurchasing} from "../../repository/actions/orderActions";
import Modal from "../../components/UI/Modal/Modal";
import CheckOut from "../CheckOut/CheckOut";

const MenuOrder = () => {
    const dispatch = useDispatch();
    const menu = useSelector(state => state.menu.menu);
    const order = useSelector(state => state.order);
    const loading = useSelector(state => state.menu.fetchLoading);
    useEffect(() => {
        dispatch(fetchMenu());
    }, [dispatch]);

    const addDishHandler = (dishName, dishPrice) => {
        dispatch(addDish(dishName, dishPrice));
    }
    const removeDishHandler = (dishName, dishPrice) => {
        dispatch(removeDish(dishName, dishPrice));
    }
    const purchaseHandler = () => {
        dispatch(setPurchasing(true));
    }
    const purchaseCancelHandler = () => {
        dispatch(setPurchasing(false));

    }

    let menuOutput = menu.map(menuItem => (
        <MenuItem
            key={menuItem.id}
            name={menuItem.name}
            price={menuItem.price}
            url={menuItem.url}
            addedDish={() => addDishHandler(menuItem.name, menuItem.price)}
        />
    ));

    if (loading) {
        menuOutput = <Spinner/>;
    }

    return (
        <>
            <Modal show={order.purchasing}
                   closed={purchaseCancelHandler}
            >
                <CheckOut/>
            </Modal>
            <div className="row">
                <div className="col-sm-8">
                    <div className="row">
                        {menuOutput}
                    </div>
                </div>
                <div className="col-sm-4">
                    <OrderSummary
                        order={order}
                        removedDish={removeDishHandler}
                        purchaseHandler={purchaseHandler}
                    />
                </div>
            </div>
        </>
    );
};

export default MenuOrder;