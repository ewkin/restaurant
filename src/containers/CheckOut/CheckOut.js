import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createOrder, setPurchasing} from "../../repository/actions/orderActions";
import {fetchMenu} from "../../repository/actions/menuActions";


const CheckOut = () => {
    const dispatch = useDispatch();
    const orders = useSelector(state => state.order);

    const [customer, setCustomer] = useState({
        name: '',
        street: '',
        phone: '',
    });

    const customerDataChanged = event => {
        const {name, value} = event.target;

        setCustomer(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const orderHandler = event => {
        event.preventDefault();
        let orderToDeliver = {}
        Object.keys(orders.order).map(value => {
            if (orders.order[value].quantity > 0) {
                orderToDeliver = {...orderToDeliver, [value]: orders.order[value].quantity}
                return null;
            }
            return null;
        });

        const order = {
            orderToDeliver,
            customer: {...customer}
        };
        dispatch(createOrder(order));
        dispatch(setPurchasing(false));
        dispatch(fetchMenu());
    }

    return (
        <form onSubmit={orderHandler}>
            <div className="mb-3">
                <label htmlFor="name" className="form-label">Name</label>
                <input required type="text" className="form-control" id="name" name="name"
                       placeholder="Your name" value={customer.name}
                       onChange={customerDataChanged}/>
            </div>
            <div className="mb-3">
                <label htmlFor="street" className="form-label">Address</label>
                <input required type="text" className="form-control" id="street" name="street"
                       placeholder="Your Address" value={customer.postal}
                       onChange={customerDataChanged}/>
            </div>
            <div className="mb-3">
                <label htmlFor="phone" className="form-label">Phone</label>
                <input required type="tel" className="form-control" id="phone" name="phone"
                       placeholder="Your Phone" value={customer.phone}
                       onChange={customerDataChanged}/>
            </div>
            <button type='submit' className="btn btn-success">Order</button>
        </form>
    );
};

export default CheckOut;