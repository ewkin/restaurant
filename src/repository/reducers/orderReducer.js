import {
    ADD_DISH,
    INIT_ORDER,
    ORDER_FAILURE,
    ORDER_REQUEST,
    ORDER_SUCCESS,
    REMOVE_DISH,
    SET_PURCHASING
} from "../actions/orderActions";

const initialState = {
    order: {},
    totalPrice: 4,
    purchasing: false,
    loading: false,
    error: null,
};

export const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case INIT_ORDER:
            return {...state, order: action.initOrder, totalPrice: 4};
        case ORDER_REQUEST:
            return {...state, loading: true};
        case ORDER_SUCCESS:
            return {...state, loading: false, ordered: true};
        case ORDER_FAILURE:
            return {...state, loading: false, error: action.error};
        case ADD_DISH:
            return {
                ...state, order: {
                    ...state.order,
                    [action.dishName]: {
                        quantity: state.order[action.dishName].quantity + 1,
                        price: state.order[action.dishName].price + action.dishPrice
                    }
                }, totalPrice: state.totalPrice + action.dishPrice
            };
        case REMOVE_DISH:
            if (state.totalPrice <= 0) return state;
            return {
                ...state, order: {
                    ...state.order, [action.dishName]: {quantity: 0, price: 0}
                }, totalPrice: state.totalPrice - action.dishPrice
            };
        case SET_PURCHASING:
            return {...state, purchasing: action.purchasing};
        default:
            return state;
    }
};