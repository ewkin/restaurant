import {FETCH_MENU_REQUEST, FETCH_MENU_FAILURE, FETCH_MENU_SUCCESS} from "../actions/menuActions";


const initialState = {
    fetchLoading: false,
    fetchError: false,
    menu: []
};

export const menuReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MENU_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_MENU_SUCCESS:
            return {...state, menu: action.menu, fetchLoading: false};
        case FETCH_MENU_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.error};
        default:
            return state;
    }
};