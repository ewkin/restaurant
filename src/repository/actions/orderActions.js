import {axiosRestaurant} from "../../axios-Restaurant";

export const ADD_DISH = 'ADD_DISH';
export const REMOVE_DISH = 'REMOVE_DISH';
export const SET_PURCHASING = 'SET_PURCHASING';
export const INIT_ORDER = 'INIT_ORDER';

export const ORDER_REQUEST = 'ORDER_REQUEST';
export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_FAILURE = 'ORDER_FAILURE';

export const orderRequest = () => ({type: ORDER_REQUEST});
export const orderSuccess = () => ({type: ORDER_SUCCESS});
export const orderFailure = error => ({type: ORDER_FAILURE, error});

export const addDish = (dishName, dishPrice) => ({type: ADD_DISH, dishName, dishPrice});
export const removeDish = (dishName, dishPrice) => ({type: REMOVE_DISH, dishName, dishPrice});
export const setPurchasing = purchasing => ({type: SET_PURCHASING, purchasing});
export const initOrder = initOrder => ({type: INIT_ORDER, initOrder});

export const createOrder = order => {
    return async dispatch => {
        try {
            dispatch(orderRequest());
            await axiosRestaurant.post('/orders.json', order);
            dispatch(orderSuccess());
        } catch (e) {
            dispatch(orderFailure(e));
        }
    };
};