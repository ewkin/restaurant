import {axiosRestaurant} from "../../axios-Restaurant";
import {initOrder} from "./orderActions";

export const FETCH_MENU_REQUEST = 'FETCH_MENU_REQUEST';
export const FETCH_MENU_SUCCESS = 'FETCH_MENU_SUCCESS';
export const FETCH_MENU_FAILURE = 'FETCH_MENU_FAILURE';

export const fetchMenuRequest = () => ({type: FETCH_MENU_REQUEST});
export const fetchMenuSuccess = menu => ({type: FETCH_MENU_SUCCESS, menu});
export const fetchMenuFailure = error => ({type: FETCH_MENU_FAILURE, error});

export const fetchMenu = () => {
    return async dispatch => {
        try {
            dispatch(fetchMenuRequest());
            const response = await axiosRestaurant.get('/menu.json');
            const menu = Object.keys(response.data).map(id => ({
                ...response.data[id], id
            }))
            dispatch(fetchMenuSuccess(menu));
            let initOrderData = {}
            for (let i = 0; i < menu.length; i++) {
                initOrderData = {...initOrderData, [menu[i].name]: {quantity: 0, price: 0}};
            }
            ;

            dispatch(initOrder(initOrderData));
        } catch (error) {
            dispatch(fetchMenuFailure(error));
        }
    };
};