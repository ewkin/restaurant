import React from 'react';

const MenuItem = ({name, price, url, addedDish}) => {

    return (
        <div className="col-sm-6">
            <div className="card">
                <img src={url} className="card-img-top" alt="Dish"/>
                <div className="card-body">
                    <h5 className="card-title">{name}</h5>
                    <p className="card-text">Price: ${price}</p>
                    <button onClick={addedDish} className="btn btn-primary">Add to Cart</button>
                </div>
            </div>
        </div>
    );
};

export default MenuItem;