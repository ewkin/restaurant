import React from 'react';

const OrderSummary = ({order, removedDish, purchaseHandler}) => {

    let orders = Object.keys(order.order).map(key => {
        if (order.order[key].quantity > 0) {
            return <li key={key} className="list-group-item">{key} x {order.order[key].quantity} for
                ${order.order[key].price}
                <button onClick={()=>removedDish(key, order.order[key].price)} className="btn btn-danger">-</button>
            </li>
        } else {
            return null
        }

    });
    if (order.totalPrice <= 4) {
        orders = (<p>Add something to the cart</p>);
    }

    return (
        <div className="card">
            <div className="card-body">
                <h5 className="card-title">Your Order</h5>
            </div>
            <ul className="list-group list-group-flush">
                {orders}
                <li  className="list-group-item">
                    Delivery: $4 Total Price: ${order.totalPrice}
                </li>
            </ul>
            <div className="card-body">
                <button disabled={!(order.totalPrice>4)} onClick={purchaseHandler} className="btn btn-success">Place Order</button>
            </div>
        </div>
    );
};

export default OrderSummary;