import axios from 'axios';

export const axiosRestaurant = axios.create({
    baseURL: 'https://restaurant-menu-83e59-default-rtdb.firebaseio.com/'
});

